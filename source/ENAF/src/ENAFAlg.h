#ifndef MYATHENAXAODANALYSIS_MYATHENAXAODANALYSISALG_H
#define MYATHENAXAODANALYSIS_MYATHENAXAODANALYSISALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "SUSYTools/ISUSYObjDef_xAODTool.h"

// macros
#include "defs.h"

class ENAFAlg : public ::AthAnalysisAlgorithm {
public:
  ENAFAlg(const std::string &name, ISvcLocator *pSvcLocator);
  virtual ~ENAFAlg();

  /// uncomment and implement methods as required

  // IS EXECUTED:
  virtual StatusCode initialize(); // once, before any input is loaded
  virtual StatusCode
  beginInputFile(); // start of each input file, only metadata loaded
  // virtual StatusCode  firstExecute();   //once, after first eventdata is
  // loaded (not per file)
  virtual StatusCode execute(); // per event
  // virtual StatusCode  endInputFile();   //end of each input file
  // virtual StatusCode  metaDataStop();   //when outputMetaStore is populated
  // by MetaDataTools
  virtual StatusCode finalize(); // once, after all events processed

private:
  // Tree variables
  DEFINE_MEMBERS;

  ToolHandle<ST::ISUSYObjDef_xAODTool> m_SUSYTools;
  IChronoStatSvc* m_chrono;
};

#endif //> !MYATHENAXAODANALYSIS_MYATHENAXAODANALYSISALG_H
