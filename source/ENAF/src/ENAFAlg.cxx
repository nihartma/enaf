#include "ENAFAlg.h"
#include "xAODEventInfo/EventInfo.h"

ENAFAlg::ENAFAlg(const std::string &name,
                                                 ISvcLocator *pSvcLocator)
    : AthAnalysisAlgorithm(name, pSvcLocator) {
  declareProperty("SUSYTools", m_SUSYTools);
}

ENAFAlg::~ENAFAlg() {}

StatusCode ENAFAlg::initialize() {
  ATH_MSG_INFO("Initializing " << name() << "...");

  ATH_CHECK(m_SUSYTools.retrieve());
  ATH_MSG_INFO("Retrieved tool: " << m_SUSYTools->name());

  ATH_CHECK(book(TTree("analysis", "My analysis ntuple")));

  auto mytree = tree("analysis");
  ADD_BRANCHES;

  m_chrono = chronoSvc();

  return StatusCode::SUCCESS;
}

StatusCode ENAFAlg::finalize() {
  ATH_MSG_INFO("Finalizing " << name() << "...");
  return StatusCode::SUCCESS;
}

StatusCode ENAFAlg::execute() {

  m_chrono->chronoStart("event");

  ATH_MSG_DEBUG("Executing " << name() << "...");
  setFilterPassed(false);

  xAOD::ElectronContainer *electrons_nominal(0);
  xAOD::ShallowAuxContainer *electrons_nominal_aux(0);
  ATH_CHECK(m_SUSYTools->GetElectrons(electrons_nominal, electrons_nominal_aux,
                                      true, "AnalysisElectrons"));
  ATH_MSG_DEBUG("Number of electrons: " << electrons_nominal->size());

  xAOD::MuonContainer *muons_nominal(0);
  xAOD::ShallowAuxContainer *muons_nominal_aux(0);
  ATH_CHECK(m_SUSYTools->GetMuons(muons_nominal, muons_nominal_aux, true,
                                  "AnalysisMuons"));
  ATH_MSG_DEBUG("Number of muons: " << muons_nominal->size());

  xAOD::JetContainer *jets_nominal(0);
  xAOD::ShallowAuxContainer *jets_nominal_aux(0);
  ATH_CHECK(m_SUSYTools->GetJets(jets_nominal, jets_nominal_aux, true,
                                 "AnalysisJets"));
  ATH_MSG_DEBUG("Number of jets: " << jets_nominal->size());

  ATH_CHECK(m_SUSYTools->OverlapRemoval(electrons_nominal, muons_nominal,
                                        jets_nominal));

  CLEAR_VECTORS;
  for (auto jet : *jets_nominal) {
    PUSH_JETS;
  }
  for (auto electron : *electrons_nominal) {
    auto electron_track = electron->trackParticle();
    PUSH_ELECTRONS;
  }
  for (auto muon : *muons_nominal) {
    auto muon_track = muon->primaryTrackParticle();
    PUSH_MUONS;
  }
  tree("analysis")->Fill();

  setFilterPassed(true);

  m_chrono->chronoStop("event");

  return StatusCode::SUCCESS;
}

StatusCode ENAFAlg::beginInputFile() {
  return StatusCode::SUCCESS;
}
