// Definitions
#define JET_VARIABLES(MEMBER, AUX)        \
  MEMBER(jet, float, pt);                 \
  MEMBER(jet, float, eta);                \
  MEMBER(jet, float, phi);                \
  MEMBER(jet, float, e);                  \
  AUX(jet, float, Jvt);                   \
  AUX(jet, char, passOR);                 \
  AUX(jet, char, passJvt);                \
  AUX(jet, char, baseline);               \
  AUX(jet, char, signal);                 \
  AUX(jet, char, bad)

#define ELECTRON_VARIABLES(MEMBER, AUX, TRACK)  \
  MEMBER(electron, float, pt);                  \
  MEMBER(electron, float, eta);                 \
  MEMBER(electron, float, phi);                 \
  MEMBER(electron, float, e);                   \
  AUX(electron, char, passOR);                  \
  AUX(electron, char, baseline);                \
  AUX(electron, char, signal);                  \
  TRACK(electron, float, d0);                   \
  TRACK(electron, float, z0)

#define MUON_VARIABLES(MEMBER, AUX, TRACK) \
  MEMBER(muon, float, pt);                 \
  MEMBER(muon, float, eta);                \
  MEMBER(muon, float, phi);                \
  MEMBER(muon, float, e);                  \
  AUX(muon, char, passOR);                 \
  AUX(muon, char, baseline);               \
  AUX(muon, char, signal);                 \
  AUX(muon, char, bad);                    \
  TRACK(muon, float, d0);                  \
  TRACK(muon, float, z0)

// Helpers
#define DEFINE_MEMBER(part, T, name) std::vector<T> m_##part##_##name
#define ADD_BRANCH(part, T, name) mytree->Branch(#part "_" #name, &m_##part##_##name)
#define VEC_CLEAR(part, T, name)  m_##part##_##name.clear()
#define VEC_PUSH_AUX(part, T, name) m_##part##_##name.push_back(part->auxdata<T>(#name))
#define VEC_PUSH_MEMBER(part, T, name) m_##part##_##name.push_back(part->name())
#define VEC_PUSH_TRACK(part, T, name) m_##part##_##name.push_back(part##_track->name())

// These are actually used in the code
#define DEFINE_MEMBERS                                              \
  JET_VARIABLES(DEFINE_MEMBER, DEFINE_MEMBER);                      \
  ELECTRON_VARIABLES(DEFINE_MEMBER, DEFINE_MEMBER, DEFINE_MEMBER);  \
  MUON_VARIABLES(DEFINE_MEMBER, DEFINE_MEMBER, DEFINE_MEMBER)
#define CLEAR_VECTORS                                       \
  JET_VARIABLES(VEC_CLEAR, VEC_CLEAR);                      \
  ELECTRON_VARIABLES(VEC_CLEAR, VEC_CLEAR, VEC_CLEAR);      \
  MUON_VARIABLES(VEC_CLEAR, VEC_CLEAR, VEC_CLEAR)
#define ADD_BRANCHES                                        \
  JET_VARIABLES(ADD_BRANCH, ADD_BRANCH);                    \
  ELECTRON_VARIABLES(ADD_BRANCH, ADD_BRANCH, ADD_BRANCH);   \
  MUON_VARIABLES(ADD_BRANCH, ADD_BRANCH, ADD_BRANCH)

#define PUSH_JETS JET_VARIABLES(VEC_PUSH_MEMBER, VEC_PUSH_AUX)
#define PUSH_ELECTRONS ELECTRON_VARIABLES(VEC_PUSH_MEMBER, VEC_PUSH_AUX, VEC_PUSH_TRACK)
#define PUSH_MUONS MUON_VARIABLES(VEC_PUSH_MEMBER, VEC_PUSH_AUX, VEC_PUSH_TRACK)
