
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../ENAFAlg.h"

DECLARE_ALGORITHM_FACTORY( ENAFAlg )

DECLARE_FACTORY_ENTRIES( ENAF ) 
{
  DECLARE_ALGORITHM( ENAFAlg );
}
