##################################################
# SUSYTools configuration file
##################################################
Trigger.UpstreamMatching: false
#
EleBaseline.Pt: 7000
EleBaseline.Eta: 2.47
EleBaseline.Id: LooseAndBLayerLLH
EleBaseline.CrackVeto: false
EleBaseline.z0: 0.5
#
Ele.Et: 7000.
Ele.Eta: 2.47
Ele.CrackVeto: false
Ele.Iso: FCLoose
Ele.IsoHighPtThresh: 75
Ele.IsoHighPt: FCHighPtCaloOnly
#
Ele.Id: TightLLH
Ele.d0sig: 5.
Ele.z0: 0.5
# ChargeIDSelector WP
Ele.CFT: None
#
MuonBaseline.Pt: 6000
MuonBaseline.Eta: 2.7
MuonBaseline.Id: 1 # Medium
MuonBaseline.z0: 0.5
#
Muon.Pt: 6000.
Muon.Eta: 2.5
Muon.Id: 1 # Medium
Muon.Iso: FCLoose
Muon.IsoHighPtThresh: 75
Muon.IsoHighPt: FCTightTrackOnly
Muon.d0sig: 3.
Muon.z0: 0.5
#
MuonCosmic.z0: 1.
MuonCosmic.d0: 0.2
#
BadMuon.qoverp: 0.4
#
PhotonBaseline.Pt: 25000.
PhotonBaseline.Eta: 2.37
PhotonBaseline.Id: Tight
#
Photon.Pt: 130000.
Photon.Eta: 2.37
Photon.Id: Tight
Photon.Iso: FixedCutTight
#
Tau.Pt: 20000.
Tau.Eta: 2.50
Tau.Id: Medium
#Tau.DoTruthMatching: false
#
Jet.Pt: 20000.
Jet.Eta: 4.5
Jet.InputType: 9 # PFlow
Jet.JVT_WP: Medium
Jet.UncertConfig: rel21/Summer2019/R4_CategoryReduction_SimpleJER.config
#
FwdJet.doJVT: false
FwdJet.JvtEtaMin: 2.5
FwdJet.JvtPtMax: 50e3
# FatJets
Jet.LargeRcollection: AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets
Jet.WtaggerConfig: SmoothedContainedWTagger_AntiKt10LCTopoTrimmed_FixedSignalEfficiency50_MC16d_20190410.dat
Jet.ZtaggerConfig: SmoothedContainedZTagger_AntiKt10LCTopoTrimmed_FixedSignalEfficiency50_MC16d_20190410.dat
Jet.LargeRuncConfig: rel21/Summer2019/R10_CategoryReduction.config
#Jet.LargeRuncVars: pT,Tau21WTA,Split12
#
BadJet.Cut: LooseBad
#
#master switch for btagging use in ST. If false, btagging is not used neither for jets decorations nor for OR (regardless of the options below)
Btag.enable: true
#
Btag.Tagger: DL1r # DL1, DL1mu, DL1rnn, MV2c10mu, MV2c10rnn, MC2cl100_MV2c100
Btag.WP: FixedCutBEff_77
Btag.KeyOverride: AnalysisJets
Btag.TimeStamp: 201903
Btag.MinPt: 20000
#
TrackJet.Coll: AntiKtVR30Rmax4Rmin02TrackJets # AntiKt2PV0TrackJets
TrackJet.Pt: 20000.
TrackJet.Eta: 2.8
BtagTrkJet.MinPt: 10000
BtagTrkJet.Tagger: MV2c10
BtagTrkJet.WP: FixedCutBEff_77
#BtagTrkJet.TimeStamp: # 201810, 201903
#
# set the -999. to positive number to override default
OR.DoBoostedElectron: true
OR.BoostedElectronC1: -999.
OR.BoostedElectronC2: -999.
OR.BoostedElectronMaxConeSize: -999.
OR.DoBoostedMuon: true
OR.BoostedMuonC1: -999.
OR.BoostedMuonC2: -999.
OR.BoostedMuonMaxConeSize: -999.
OR.DoMuonJetGhostAssociation: true
OR.DoTau: false
OR.DoPhoton: false
OR.Bjet: false
OR.ElBjet: false
OR.MuBjet: false
OR.TauBjet: false
OR.MuJetApplyRelPt: false
OR.MuJetPtRatio: -999.
OR.MuJetTrkPtRatio: -999.
OR.RemoveCaloMuons: true
OR.MuJetInnerDR: -999.
OR.BtagWP: FixedCutBEff_85
OR.BJetPtUpperThres: -1
#
OR.DoFatJets: false
OR.EleFatJetDR: -999.
OR.JetFatJetDR: -999.
#
SigLep.RequireIso: true
#
MET.EleTerm: RefEle
MET.GammaTerm: RefGamma
MET.TauTerm: RefTau
MET.JetTerm: RefJet
MET.MuonTerm: Muons
MET.InputSuffix: AnalysisMET
MET.OutputTerm: Final
MET.JetSelection: Tight # Loose, Tight, Tighter, Tenacious
MET.RemoveOverlappingCaloTaggedMuons: true
MET.DoRemoveMuonJets: true
MET.UseGhostMuons: false
MET.DoMuonEloss: false
#
# Trigger SFs configuration
Ele.TriggerSFStringSingle: SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0
#
# actual Mu files have to be set in SUSYTools
PRW.ActualMu2017File: GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRW.ActualMu2018File: GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
# default to None but do set to BFilter (366010-366017), CFilterBVeto (366019-366026), or CVetoBVeto (366028-366035) to remap MC16e Znunu dsid
PRW.autoconfigPRWHFFilter: None
#
StrictConfigCheck: true
