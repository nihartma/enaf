jps.AthenaCommonFlags.AccessMode = "ClassAccess"

alg = CfgMgr.ENAFAlg()
athAlgSeq += alg

#svcMgr.EventSelector.InputCollections = ["root://lcg-lrz-rootd.grid.lrz.de:1094/pnfs/lrz-muenchen.de/data/atlas/dq2/atlaslocalgroupdisk/rucio/user/elmsheus/e7/9e/user.elmsheus.21230205.EXT0._000002.DAOD_PHYSLITE.mc.pool.root"]

svcMgr.EventSelector.InputCollections = ["/home/nikolai/data/user.elmsheus.21230205.EXT0._000002.DAOD_PHYSLITE.mc.pool.root"]

# see
# https://gitlab.cern.ch/atlas/athena/-/blob/21.2/PhysicsAnalysis/SUSYPhys/SUSYTools/share/minimalExampleJobOptions_mc.py
ToolSvc += CfgMgr.ST__SUSYObjDef_xAOD("SUSYTools")
ToolSvc.SUSYTools.ConfigFile = "ENAF/SUSYTools_SusySkim1LInclusive_testPhysLite.conf"
alg.SUSYTools = ToolSvc.SUSYTools

svcMgr.MessageSvc.Format = "% F%50W%S%7W%R%T %0W%M" #Creates more space for displaying tool names
svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=100)

jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:ST_ntuple_w_impact.root"]

#include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above
