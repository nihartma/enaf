# Enaf is Not A Framework

It's just a small athena skeleton that dumps ntuples with decorators from SUSYTools.

## How to compile

```
mkdir build
cd build
acmSetup AthAnalysis,21.2.124
acm compile
```

## How to run

```
athena ENAF/ENAFAlgJobOptions.py
```

adjust the job options if you like (e.g. for using a different SUSYTools config file)

## The branches

... are hardcoded in [defs.h](source/ENAF/src/defs.h). Edit and recompile if you wish different branches